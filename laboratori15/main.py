import time

from celery import Celery
from celery.schedules import crontab

app = Celery(
    name="tasks",
    broker="redis://localhost:6379",
    backend="db+sqlite:///db.sqlite3"
)


@app.task()
def process_data_a(a, b):
    time.sleep(8)
    return a + b


@app.task()
def process_data_b(a, b):
    time.sleep(1)
    resultado = a + b
    print('hola, resultado de la calibracion' + resultado)
    return a + b


def process():
    print("procesando...")
    process_data_a.delay(5, 5)
    process_data_b.delay(3, 8)
    print("te avisaremos luego...")


app.conf.beat_schedule = {
    'add-every': {
        'task': 'main.process_data_a',
        'schedule': crontab(minute='*/1'),
        'args': (7, 4)
    }
}
