from django.http import HttpResponse
from django.views import View
from score.core.models import Scores


class ScoresView(View):
    def get(self, *args):
        score = Scores.objects.get(pk=1)
        return HttpResponse(f"<p>Respuesta: {score.predict}</p>")

    def public(self):
        return {
            "score": 20,
            "origen": "Oscar Mendez"
        }
