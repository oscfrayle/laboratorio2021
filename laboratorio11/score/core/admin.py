from django.contrib import admin
from score.core.models import Scores


class ScoresAdmin(admin.ModelAdmin):
    pass


admin.site.register(Scores, ScoresAdmin)
