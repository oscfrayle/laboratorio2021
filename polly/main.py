import boto3
from contextlib import closing
client = boto3.client('polly')
response = client.synthesize_speech(
    OutputFormat='mp3',
    LanguageCode='es-MX',
    Text='<speak>Soy Cachetines<break time="0.5s"/> tu devops virtual <emphasis>Zinobe</emphasis>. <break time="1s"/> Marca 1 si quieres hablar con migo</speak>',
    TextType='ssml',
    VoiceId='Miguel'
)

if "AudioStream" in response:
    with closing(response["AudioStream"]) as stream:
        output = "polly-boto-Miguel.mp3"

        try:
            with open(output, "wb") as file:
                file.write(stream.read())
        except IOError as error:
            print(error)
            sys.exit(-1)
