import json
import pickle


def hello(event, context):

    print(event["queryStringParameters"]["oreja"])
    with open("modelod.pkl", "rb") as file:
        modelo = pickle.load(file)

    tipo = modelo.predict([[
        event["queryStringParameters"]["oreja"],
        event["queryStringParameters"]["panza"], 3, 1]])
    print(tipo)

    response = {
        "statusCode": 200,
        "body": json.dumps(
            {
                "tipo": str(tipo[0])
            }
        )
    }

    return response

    # Use this code if you don't use the http event with the LAMBDA-PROXY
    # integration
    """
    return {
        "message": "Go Serverless v1.0! Your function executed successfully!",
        "event": event
    }
    """
