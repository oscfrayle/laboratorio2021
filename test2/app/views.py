class Users:
    def __init__(self, name=None):
        self.name = name

    def get_name(self):
        return self.name

    def get_age(self):
        return 100


class Products:
    def __init__(self):
        pass

    def get_name(self):
        return "arroz"

    def get_price(self):
        return "$2000"


class Bills:
    def get_all(self):
        return [1, 2, 3, 4]

    def get_reference(self):
        return "1234"
