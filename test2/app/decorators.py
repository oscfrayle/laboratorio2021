def suma_fake(function):
    def wrapper(num):
        if num % 2 == 0:
            return function(num)
        else:
            num = 0
            return function(num)
    return wrapper
