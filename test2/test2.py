class TestClass:
    def __init__(self):
        print("hola 3")

    def __new__(cls):
        print("hola 2")
        return super(TestClass, cls).__new__(cls)

    def __call__(self, a):
        print("hola 1 " + str(a))



class Single:
    instance = None
    def __new__(self):
        if Singleton.instance is None:
            Singleton.instance = object.__new__(cls)
        return Singleton.instance

    def __str__(self):
        return 'hola'


class Singleton:
    instance = None

    def __new__(cls):
        if Singleton.instance is None:
            Singleton.instance = object.__new__(cls)
        return Singleton.instance

    def __str__(self):
        return 'hola'


obja = Singleton()
objb = Singleton()
objc = Singleton()
print(id(obja))
print(id(objb))
print(id(objc))

print("=====")


class SingletonSin:
    def __str__(self):
        return 'hola'


obj2 = SingletonSin()
obj3 = SingletonSin()
obj4 = SingletonSin()
print(id(obj2))
print(id(obj3))
print(id(obj4))


class Factory:
    def __init__(self, instance= None):
        self.instance = instance
        self.base = 'app.views'

    def get_instance(self):
        if self.instance == None:
            raise "Error..."
        path = f"{self.base}.{self.instance}".split(".")
        module = __import__(
            path[:-1]
        )
        for component in path[:1]:
            module = getattr(module, component)
        return module
